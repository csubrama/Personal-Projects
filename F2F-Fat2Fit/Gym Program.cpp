#include<iostream>
#include<conio.h>
#include<stdio.h>
#include<math.h>
#include<fstream>
#include<windows.h>
using namespace std;
class ainfo
{
	public:
	float cholestrol;
	float sugar;
	float bmi;
	float bmd;
	char allergies[20]; 
	void getdata();
};
void ainfo::getdata()
{
	Beep(1000,500);
	Beep(2000,500);
	Beep(1000,500);
	Beep(2000,500);
	Beep(1000,500);
	Beep(2000,500);
	cout<<"___________________ADVANCED INFORMATION________________"<<endl;
	cout<<"Cholestrol Level(LDL only): ";
	cin>>cholestrol;
	cout<<"Sugar Level(in mmol/L): ";
	cin>>sugar;
	cout<<"Your BMI: ";
	cin>>bmi;
	cout<<"Your BMD: ";
	cin>>bmd;
	cout<<"Allergies(if any): ";
	cin>>allergies;
	cout<<"_____________THANK YOU FOR YOUR COOPERATION_____________"<<endl<<endl;
	Beep(1000,500);
	Beep(2000,500);
	Beep(1000,500);
	Beep(2000,500);
	Beep(1000,500);
	Beep(2000,500);
}
class info
{
	public:
	int age;
	float height;
	char name[20];
	float weight1;
	char gender;
	void getdata();
	void putdata();
};
void info::getdata()
{
	cout<<"___________________BASIC INFORMATION________________"<<endl;
	cout<<"Enter your name"<<endl;
	cin>>name;
	cout<<"Enter your age"<<endl;
	cin>>age;
	cout<<"Enter your gender(M or F)"<<endl;
	Y:
	cin>>gender;
	if((gender=='m')||(gender=='f')||(gender=='male')||(gender=='female')||(gender=='Male')||(gender=='Female'))
	{
		cout<<"Enter caps M or F only"<<endl;
		goto Y;
	}
	cout<<"What is your height(in cms)"<<endl;
	cin>>height;
	cout<<"Enter your weight(in kg)"<<endl;
	cin>>weight1;
	cout<<"______________________Thank You______________________"<<endl<<endl;
}
class goal
{
	public:
	int choice1;
	int choice3;
	float weight2;
	float pecs;
	int time;
	float biceps;
	float quads;
	void getdata();
};
void goal::getdata()
{
	cout<<"In this step we will be focusing on your goals..."<<endl<<endl;
	cout<<"Press 1 to lose weight"<<endl;
	cout<<"Press 2 to gain weight"<<endl;
	cout<<"Press 3 to maintain weight"<<endl;
	X:
	cin>>choice1;
	if(choice1==1)
	{
		cout<<"How much weight do you want to lose(in kg)?"<<endl;
		cin>>weight2;
	}
	else
	if(choice1==2)
	{
		cout<<"how much weight do you want to gain(in kg)"<<endl;
		cin>>weight2;
	}
	else
	if(choice1>=3)
	{
		cout<<"Please enter a correct option"<<endl;
		goto X;
	}
	cout<<"Press 1 to work on muscles also"<<endl;
	cout<<"Press 2 to continue"<<endl;
	cin>>choice3;
	if(choice3==1)
	{
		cout<<"In how many weeks do you want to do so?"<<endl;
	    cin>>time;
	    cout<<"Enter desired size of pectoralis(in inches)"<<endl;
	    cin>>pecs;
	    cout<<"Enter desired size of biceps(in inches)"<<endl;
	    cin>>biceps;
	    cout<<"Enter desired size of quadriceps(in inches)"<<endl;
	    cin>>quads;
    }
}
class workout:public goal
{
	public:
		void getdata();
};
void workout::getdata()
{
	int choice2;
	goal::getdata();
	cout<<endl;
	cout<<"Based on the details enter by you"<<endl;
	cout<<"we have planned a workout for you!!"<<endl<<endl;
	Beep(1000,500);
	Beep(2000,500);
	Beep(1000,500);
	Beep(2000,500);
	if(choice1==1)
	{
		cout<<"                         YOUR WORKOUT                               "<<endl;
		cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		cout<<"Goal: Lose "<<weight2<<" kg's"<<endl;
		cout<<"Cardio workout:"<<endl;
		cout<<"\t"<<"\t"<<"Treadmill: Inclination-2, Speed-8.5Km\hr"<<endl;
		cout<<"\t"<<"\t"<<"Skipping: 30 reps, 3 sets"<<endl;
		cout<<"Chest workout:"<<endl;
		cout<<"\t"<<"\t"<<"Chest press: 15-reps, 3 sets"<<endl;
		cout<<"\t"<<"\t"<<"Chest press(fly): 15-reps, 3 sets"<<endl;
		cout<<"Bicep workout:"<<endl;
		cout<<"\t"<<"\t"<<"Bicep curls: 15-reps, 3 sets"<<endl;
		cout<<"\t"<<"\t"<<"Horizontal pull: 15-reps, 3 sets"<<endl;
		cout<<"Quadriceps workout:"<<endl;
		cout<<"\t"<<"\t"<<"Quad. press: 15-reps, 3 sets"<<endl;
		cout<<"\t"<<"\t"<<"Cycling: 2 kms daily"<<endl;
		cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		cout<<"By doing the above workout you will be able to lose weight!!"<<endl;
		cout<<"Press 1 if you agree to this workout"<<endl;
		cout<<"Press 2 if you refuse to this workout"<<endl;
		cin>>choice2;
		cout<<endl;
		if(choice2==2)
		{
			cout<<"It seems you did not like the workout...";
			cout<<"Let us see if we can formulate another one: "<<endl;
			Beep(1000,500);
	        Beep(2000,500);
	        Beep(1000,500);
	        Beep(2000,500);
		    cout<<"Okay here is another one: "<<endl;
		    
		    cout<<"                         YOUR WORKOUT                               "<<endl;
		    cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		    cout<<"Goal: Gain "<<weight2<<" kg's"<<endl;
		    cout<<"Cardio workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Cross-Fit: Mode-Manual, Speed-8.5Km\hr"<<endl;
		    cout<<"\t"<<"\t"<<"Zumba: 30-45 minutes, intense workout"<<endl;
		    cout<<"Chest workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Push-ups: 15-reps, 3 sets"<<endl;
		    cout<<"\t"<<"\t"<<"Chest press(fly): 15-reps, 3 sets"<<endl;
		    cout<<"Bicep workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Dumbells: 15-reps, 3 sets"<<endl;
		    cout<<"\t"<<"\t"<<"Barbells: 15-reps, 3 sets"<<endl;
		    cout<<"Quadriceps workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Quad. press: 15-reps, 3 sets"<<endl;
		    cout<<"\t"<<"\t"<<"Cycling: 2 kms daily"<<endl;
		    cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		    cout<<"By doing the above workout you will be able to lose weight!!"<<endl;
		    cout<<"Press any key to continue"<<endl;
		    getch();
		    cout<<endl<<endl;
		}
	}
	else
    if(choice1==2)
	{
		cout<<"                         YOUR WORKOUT                               "<<endl;
		cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		cout<<"Goal: Gain "<<weight2<<" kg's"<<endl;
		cout<<"Cardio workout:"<<endl;
		cout<<"\t"<<"\t"<<"Treadmill: Inclination-2, Speed-8.5Km\hr"<<endl;
		cout<<"\t"<<"\t"<<"Skipping: 30 reps, 3 sets"<<endl;
		cout<<"Chest workout:"<<endl;
		cout<<"\t"<<"\t"<<"Chest press: 15-reps, 3 sets"<<endl;
		cout<<"\t"<<"\t"<<"Chest press(fly): 15-reps, 3 sets"<<endl;
		cout<<"Bicep workout:"<<endl;
		cout<<"\t"<<"\t"<<"Bicep curls: 15-reps, 3 sets"<<endl;
		cout<<"\t"<<"\t"<<"Horizontal pull: 15-reps, 3 sets"<<endl;
		cout<<"Quadriceps workout:"<<endl;
		cout<<"\t"<<"\t"<<"Quad. press: 15-reps, 3 sets"<<endl;
		cout<<"\t"<<"\t"<<"Cycling: 2 kms daily"<<endl;
		cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		cout<<"By doing the above workout you will be able to gain weight!!"<<endl;
		cout<<"Press 1 if you agree to this workout"<<endl;
		cout<<"Press 2 if you refuse to this workout"<<endl;
		cin>>choice2;
		cout<<endl;
		if(choice2==2)
		{
			cout<<"It seems you did not like the workout...";
			cout<<"Let us see if we can formulate another one: "<<endl;
			Beep(1000,500);
	        Beep(2000,500);
	        Beep(1000,500);
	        Beep(2000,500);
		    cout<<"Okay here is another one: "<<endl;
		    
		    cout<<"                         YOUR WORKOUT                               "<<endl;
		    cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		    cout<<"Goal: Gain "<<weight2<<" kg's"<<endl;
		    cout<<"Cardio workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Cross-Fit: Mode-Manual, Speed-8.5Km\hr"<<endl;
		    cout<<"\t"<<"\t"<<"Zumba: 30-45 minutes, intense workout"<<endl;
		    cout<<"Chest workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Push-ups: 15-reps, 3 sets"<<endl;
		    cout<<"\t"<<"\t"<<"Chest press(fly): 15-reps, 3 sets"<<endl;
		    cout<<"Bicep workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Dumbells: 15-reps, 3 sets"<<endl;
		    cout<<"\t"<<"\t"<<"Barbells: 15-reps, 3 sets"<<endl;
		    cout<<"Quadriceps workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Quad. press: 15-reps, 3 sets"<<endl;
		    cout<<"\t"<<"\t"<<"Cycling: 2 kms daily"<<endl;
		    cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		    cout<<"By doing the above workout you will be able to gain weight!!"<<endl;
		    cout<<"Press any key to continue"<<endl;
		    getch();
		    cout<<endl<<endl;
		}
	}
	else
	if(choice1==3)
	{
		cout<<"                         YOUR WORKOUT                               "<<endl;
		cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		cout<<"Goal: Maintain "<<weight2<<" kg's"<<endl;
		cout<<"Cardio workout:"<<endl;
		cout<<"\t"<<"\t"<<"Treadmill: Inclination-0, Speed-6.5Km\hr"<<endl;
		cout<<"\t"<<"\t"<<"Walking: 2.5 to 3.0 kms daily"<<endl;
		cout<<"Chest workout:"<<endl;
		cout<<"\t"<<"\t"<<"Pushups: 10-reps, 3 sets"<<endl;
		cout<<"\t"<<"\t"<<"Chest press(fly): 15-reps, 3 sets"<<endl;
		cout<<"Bicep workout:"<<endl;
		cout<<"\t"<<"\t"<<"Lower Deltoid Pull: 15-reps, 3 sets"<<endl;
		cout<<"\t"<<"\t"<<"Horizontal pull: 15-reps, 3 sets"<<endl;
		cout<<"Quadriceps workout:"<<endl;
		cout<<"\t"<<"\t"<<"Stair climb: 15-reps, 3 sets"<<endl;
		cout<<"\t"<<"\t"<<"Cycling: 2 kms daily"<<endl;
		cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		cout<<"By doing the above workout you will be able to maintain your weight!!"<<endl;
		cout<<"Press 1 if you agree to this workout"<<endl;
		cout<<"Press 2 if you refuse to this workout"<<endl;
		cin>>choice2;
		cout<<endl;
		if(choice2==2)
		{
			cout<<"It seems you did not like the workout...";
			cout<<"Let us see if we can formulate another one: "<<endl;
			Beep(1000,500);
	        Beep(2000,500);
	        Beep(1000,500);
 	        Beep(2000,500);
		    cout<<"Okay here is another one: "<<endl;
		    
		    cout<<"                         YOUR WORKOUT                               "<<endl;
		    cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		    cout<<"Goal: Gain "<<weight2<<" kg's"<<endl;
		    cout<<"Cardio workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Cross-Fit: Mode-Manual, Speed-8.5Km\hr"<<endl;
		    cout<<"\t"<<"\t"<<"Zumba: 30-45 minutes, intense workout"<<endl;
		    cout<<"Chest workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Push-ups: 15-reps, 3 sets"<<endl;
		    cout<<"\t"<<"\t"<<"Chest press(fly): 15-reps, 3 sets"<<endl;
		    cout<<"Bicep workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Dumbells: 15-reps, 3 sets"<<endl;
		    cout<<"\t"<<"\t"<<"Barbells: 15-reps, 3 sets"<<endl;
		    cout<<"Quadriceps workout:"<<endl;
		    cout<<"\t"<<"\t"<<"Quad. press: 15-reps, 3 sets"<<endl;
		    cout<<"\t"<<"\t"<<"Cycling: 2 kms daily"<<endl;
		    cout<<"||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"<<endl;
		    cout<<"By doing the above workout you will be able to achive you goals!!"<<endl;
		    cout<<"Press any key to continue"<<endl;
		    getch();
		    cout<<endl<<endl;
		}
	}
}
class cc:public info,public workout
{
	float calorie;
	public:
		void getdata();
};
void cc::getdata()
{
	info::getdata();
	workout::getdata();
	cout<<endl;
	for(long int a=0;a<10000000;a++)
	{
	}
	cout<<"_______________________________________"<<endl;
	cout<<"           CALORIE CALCULATOR"<<endl<<endl;
	cout<<"The calorie calculator helps one to know"<<endl;
	cout<<"how many calories one has to intake to "<<endl;
	cout<<"maintain his/her weight"<<endl;
	cout<<"________________________________________"<<endl;
	cout<<"Press any key to continue"<<endl;
	getch();
	cout<<"You're details are as follows: "<<endl;
	cout<<"|||||||||||||||||||||||||||||||||||||||"<<endl;
	cout<<"Name: "<<name<<endl;
	cout<<"Age: "<<age<<endl;
	cout<<"Gender: "<<gender<<endl;
	cout<<"Weight: "<<weight1<<endl;
	cout<<"Height: "<<height<<endl<<endl;
	cout<<"|||||||||||||||||||||||||||||||||||||||"<<endl;
	cout<<"Press any key to continue";
	getch();
	cout<<endl;
	for(long int a=0;a<3000000;a++)
	{
	}
	cout<<"\nHere are the results:"<<endl;
	if(gender=='M')
	{
		cout<<"______________________________________________________________"<<endl<<endl;
		calorie=(66.47+ (13.75 * weight2) + (5.0 * height) - (6.75 * age));
		cout<<"You need "<<calorie<<" calories to maintain your weight"<<endl;
		cout<<"______________________________________________________________"<<endl<<endl;
	}
	else
	if(gender=='F')
	{
		cout<<"______________________________________________________________"<<endl;
		calorie=(665.09 + (9.56 * weight2) + (1.84 * height) - (4.67 * age));
		cout<<"You need "<<calorie<<" calories to maintain your weight"<<endl;
		cout<<"______________________________________________________________"<<endl<<endl;
	}	
	cout<<"\nPress any key to continue"<<endl;
	cout<<endl<<endl;
	getch();
}
class nutrition:public ainfo
{
	public:
		void getdata();
};
void nutrition::getdata()
{
	ainfo::getdata();
	if ((cholestrol>=240)&&(sugar>=6.5))
	{
		cout<<"Result: "<<endl;
		cout<<"\t"<<"\t"<<"High Cholesterol"<<endl;
		cout<<"\t"<<"\t"<<"Type-1 Diabetes"<<endl<<endl;
		
		cout<<"Based on you body condition we"<<endl;
		cout<<"have formulated a nutrition plan"<<endl<<endl;
		cout<<"________________NUTRITION PLAN______________"<<endl<<endl;
		cout<<"Breakfast: Non-fat dairy products"<<endl;
		cout<<"Lunch: Lean meats and poultry"<<endl;
		cout<<"Dinner: Beans(steamed) and fruits"<<endl;
		cout<<"Snacks: Rusk and sugar-less coffee"<<endl;
		cout<<"Exercise: DO ONLY CARDIO"<<endl;
		cout<<"_____________________________________________"<<endl;
	}
	if ((cholestrol>=240)&&(sugar>=4.4)&&(sugar<=6.1))
	{
		cout<<"Result: "<<endl;
		cout<<"\t"<<"\t"<<"High Cholesterol"<<endl;
		cout<<"\t"<<"\t"<<"Type-2 Diabetes"<<endl<<endl;		
		cout<<"Based on you body condition we"<<endl;
		cout<<"have formulated a nutrition plan"<<endl<<endl;
		cout<<"________________NUTRITION PLAN______________"<<endl<<endl;
		cout<<"Breakfast: Non-fat dairy products"<<endl;
		cout<<"Lunch: Lean meats and poultry"<<endl;
		cout<<"Snacks: Rusk and sugar-less coffee"<<endl;
		cout<<"Dinner: Beans(steamed) and fruits"<<endl;
		cout<<"Exercise: DO ONLY CARDIO"<<endl;
		cout<<"_____________________________________________"<<endl;
	}
	else 
	if((cholestrol<=239)&&(cholestrol>200)&&(sugar>=6.5))
	{
		cout<<"Result: "<<endl;
		cout<<"\t"<<"\t"<<"Boder-line Cholesterol"<<endl;
		cout<<"\t"<<"\t"<<"Type-1 Diabetes"<<endl<<endl;
		cout<<"Based on you body condition we"<<endl;
		cout<<"have formulated a nutrition plan"<<endl<<endl;
		cout<<"________________NUTRITION PLAN______________"<<endl<<endl;
		cout<<"Breakfast: Non-fat dairy products"<<endl;
		cout<<"Lunch: Lean meats and poultry"<<endl;
		cout<<"Snacks: Rusk and sugar-less coffee"<<endl;
		cout<<"Dinner: Beans(steamed) and fruits"<<endl;
		cout<<"Exercise: DO ONLY CARDIO"<<endl;
		cout<<"_____________________________________________"<<endl;
	}
	else
	if ((cholestrol<=239)&&(cholestrol>200)&&(sugar>=4.4)&&(sugar<=6.1))
	{
		cout<<"Result: "<<endl;
		cout<<"\t"<<"\t"<<"Boder-line cholesterol"<<endl;
		cout<<"\t"<<"\t"<<"Type-2 Diabetes"<<endl<<endl;
		
		cout<<"Based on you body condition we"<<endl;
		cout<<"have formulated a nutrition plan"<<endl<<endl;
		cout<<"________________NUTRITION PLAN______________"<<endl<<endl;
		cout<<"Breakfast: Non-fat dairy products"<<endl;
		cout<<"Lunch: Lean meats and poultry"<<endl;
		cout<<"Dinner: Beans(steamed) and fruits"<<endl;
		cout<<"Snacks: Rusk and sugar-less coffee"<<endl;
		cout<<"Exercise: DO ONLY CARDIO"<<endl;
		cout<<"_____________________________________________"<<endl;
	}
	if((cholestrol<=200)&&(sugar>=4.4)&&(sugar<=6.1))
	{
		cout<<"Result: "<<endl;
		cout<<"\t"<<"\t"<<"No Cholesterol"<<endl;
		cout<<"\t"<<"\t"<<"Type-2 Diabetes"<<endl<<endl;
		cout<<"Based on you body condition we"<<endl;
		cout<<"have formulated a nutrition plan"<<endl<<endl;
		cout<<"________________NUTRITION PLAN______________"<<endl<<endl;
		cout<<"Breakfast: Non-fat dairy products"<<endl;
		cout<<"Lunch: Lean meats and poultry"<<endl;
		cout<<"Snacks: Rusk and sugar-less coffee"<<endl;
		cout<<"Dinner: Beans(steamed) and fruits"<<endl;
		cout<<"Exercise: Do Cardio and Muscle workout"<<endl;
		cout<<"_____________________________________________"<<endl;
	}
	else
	if((cholestrol<=200)&&(sugar>=6.5))
	{
		cout<<"Result: "<<endl;
		cout<<"\t"<<"\t"<<"No Cholesterol"<<endl;
		cout<<"\t"<<"\t"<<"Type-1 Diabetes"<<endl<<endl;
		cout<<"Based on you body condition we"<<endl;
		cout<<"have formulated a nutrition plan"<<endl<<endl;
		cout<<"________________NUTRITION PLAN______________"<<endl<<endl;
		cout<<"Breakfast: Non-fat dairy products"<<endl;
		cout<<"Lunch: Lean meats and poultry"<<endl;
		cout<<"Snacks: Rusk and sugar-less coffee"<<endl;
		cout<<"Dinner: Beans(steamed) and fruits"<<endl;
		cout<<"Exercise: Do Cardio and Muscle workout"<<endl;
		cout<<"_____________________________________________"<<endl;
	}
	else
	if ((cholestrol>=240)&&(sugar<=4.3))
	{
		cout<<"Result: "<<endl;
		cout<<"\t"<<"\t"<<"High Cholesterol"<<endl;
		cout<<"\t"<<"\t"<<"No Diabetes"<<endl<<endl;
		cout<<"Based on you body condition we"<<endl;
		cout<<"have formulated a nutrition plan"<<endl<<endl;
		cout<<"________________NUTRITION PLAN______________"<<endl<<endl;
		cout<<"Breakfast: Non-fat dairy products"<<endl;
		cout<<"Lunch: Lean meats and poultry"<<endl;
		cout<<"Dinner: Beans(steamed) and fruits"<<endl;
		cout<<"Snacks: Rusk and sugar-less coffee"<<endl;
		cout<<"Exercise: DO ONLY CARDIO"<<endl;
		cout<<"_____________________________________________"<<endl;
	}
	else
	if ((cholestrol<=239)&&(cholestrol>200)&&(sugar<=4.3))
	{
		cout<<"Result: "<<endl;
		cout<<"\t"<<"\t"<<"Boder-line Cholesterol"<<endl;
		cout<<"\t"<<"\t"<<"No Diabetes"<<endl<<endl;
		cout<<"Based on you body condition we"<<endl;
		cout<<"have formulated a nutrition plan"<<endl<<endl;
		cout<<"________________NUTRITION PLAN______________"<<endl<<endl;
		cout<<"Breakfast: Non-fat dairy products"<<endl;
		cout<<"Lunch: Lean meats and poultry"<<endl;
		cout<<"Dinner: Beans(steamed) and fruits"<<endl;
		cout<<"Snacks: Rusk and sugar-less coffee"<<endl;
		cout<<"Exercise: DO ONLY CARDIO"<<endl;
		cout<<"_____________________________________________"<<endl;
	}
	else
	if ((cholestrol<=200)&&(sugar<=4.3))
	{
		cout<<"Result: "<<endl;
		cout<<"\t"<<"\t"<<"No Cholesterol"<<endl;
		cout<<"\t"<<"\t"<<"No Diabetes"<<endl<<endl;
		cout<<"Based on you body condition we"<<endl;
		cout<<"have formulated a nutrition plan"<<endl<<endl;
		cout<<"________________NUTRITION PLAN______________"<<endl<<endl;
		cout<<"Breakfast: Non-fat dairy products"<<endl;
		cout<<"Lunch: Lean meats and poultry"<<endl;
		cout<<"Dinner: Beans(steamed) and fruits"<<endl;
		cout<<"Snacks: Rusk and sugar-less coffee"<<endl;
		cout<<"Exercise: DO ONLY CARDIO"<<endl;
		cout<<"_____________________________________________"<<endl;
	}
}
main()
{
	nutrition obj2;
	cc obj1;
	ofstream fout;
	fout.open("Gym.dat",ios::out);
	cout<<"_____________CHANDU'S GYM PROGRAM-FROM FAT TO FIT____________"<<endl<<endl;
	cout<<"Please enter you information before we begin:"<<endl<<endl;
	Beep(1000,500);
	Beep(2000,500);
	Beep(1000,500);
	Beep(2000,500);
	Beep(1000,500);
	Beep(2000,500);
	for(long int a=0;a<1000000;a++)
	{
	}
	obj1.getdata();
	fout.write((char*)&obj1,sizeof(obj1));
	cout<<"_______________________________________________"<<endl<<endl;
	cout<<"For us to understand your body better"<<endl;
	cout<<"Please do fill in your advanced info"<<endl;
	cout<<"_______________________________________________"<<endl<<endl;
	obj2.getdata();
	fout.write((char*)&obj2,sizeof(obj2));
	fout.close();
	for(long int a=0;a<300000000;a++)
	{
	}
	cout<<endl<<endl;
	cout<<"By following the formulated work-out and nutrition plan"<<endl;
	cout<<"you will be able to achieve your desired goals!!"<<endl<<endl;
	cout<<"_____________CHANDU'S GYM PROGRAM-FROM FAT TO FIT____________"<<endl;
	cout<<"_________________________THANK YOU________________________"<<endl;
	getch();
}
